

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Menu from './src/components/menu';
import TelaInicial from './src/layout/telaInicial';
import SegundaTela from './src/layout/segundaTela';
import TerceiraTela from './src/layout/terceiraTela';
import QuartaTela from './src/layout/quartaTela';
import QuintaTela from './src/layout/quintaTela';


const Stack = createStackNavigator();


export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Menu" component={Menu} />
        <Stack.Screen name="TelaInicial" component={TelaInicial} />
        <Stack.Screen name="SegundaTela" component={SegundaTela} />
        <Stack.Screen name="TerceiraTela" component={TerceiraTela} />
        <Stack.Screen name="QuartaTela" component={QuartaTela} />
        <Stack.Screen name="QuintaTela" component={QuintaTela} />

      </Stack.Navigator>
    </NavigationContainer>


  );
}

