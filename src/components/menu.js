import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity } from 'react-native';



export default function Menu({navigation}) {
  return (

    <View style={styles.container}>


      <TouchableOpacity style={styles.menu}
      onPress={()=> navigation.navigate('TelaInicial')}>
        <Text>Botão 1</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.menu}
      onPress={()=> navigation.navigate('SegundaTela')}>
        <Text>Botão 2</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.menu}
      onPress={()=> navigation.navigate('TerceiraTela')}>
        <Text>Botão 3</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.menu}
      onPress={()=> navigation.navigate('QuartaTela')}>
        <Text>Botão 4</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.menu}
      onPress={()=> navigation.navigate('QuintaTela')}>
        <Text>Botão 5</Text>
      </TouchableOpacity>



    </View>

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F29BB6',
  },
  menu: {
    padding: 10,
    margin: 5,
    backgroundColor: "#B4F29B",
    borderRadius: 5,
  },

});
