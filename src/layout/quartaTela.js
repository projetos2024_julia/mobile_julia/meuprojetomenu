import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Button, TouchableOpacity, ScrollView } from 'react-native';
import { useEffect, useState } from 'react';

export default function QuartaTela() {

  return (
    
    <View style={styles.container}>
      <Text>Olá mundo!</Text>
      <Text>Hello Word!</Text>
      <Text>Hallo Welt!</Text>
      <Text>Bonjour le monde!</Text>

      
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f8ccd9',
    justifyContent: 'center',
    alignItems: 'center',

  },
});
