import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Button, TouchableOpacity, ScrollView } from 'react-native';
import { useEffect, useState } from 'react';

export default function SegundaTela() {
  const [count, setCount] = useState(0);
  

  return (
    
    <View style={styles.container}>
      
      <Text>Clique para contar: {count} </Text>
      <TouchableOpacity
      style={styles.teste}
      onPress={()=> setCount(count + 1)}
      >
      <Text>Clique Aqui!</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f8ccd9',
    alignItems: 'center',
    justifyContent: 'center',
  },

  teste: {
    color: "black",
    backgroundColor: "#ea658c",
    width:100,
    borderRadius: 20,
    alignItems: "center",
  },
});
